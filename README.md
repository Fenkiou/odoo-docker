# Odoo and Docker: the easy way

This project aims to easily setup odoo container with addons you are working on.

`create.py <odoo_container_name>`:
- `-p arg` specify the port that odoo container should listen on (default to 8069)
- `-v arg` specify the odoo version you want to use (default to 8)

`delete.py <container_name1> <container_name2> etc..`:
- delete all containers passed as arguments with their volumes


The `create.py` script automatically:
- create a database container that will be linked to the odoo container
- look inside addons directory and add them as volumes to odoo containers and modify the `addons_path` line of the configuration file.

The `openerp-server.conf` file is provided by [odoo](https://github.com/odoo/docker/blob/master/8.0/openerp-server.conf).

## TO DO:
- make a python package
- store some config from context in a file
- add an upgrade command (for images)
- create a volume for postgresql data and odoo filestore
